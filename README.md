Plugin for Atlassian Confluence to support diagram rendering with
[Mermaid](https://github.com/knsv/mermaid).

Mermaid was primarily written by Knut Sveidqvist; see file THIRDPARTY for
license info.

