package org.anvard.atlassian;

import java.util.Map;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.gzipfilter.org.apache.commons.lang.StringUtils;
import com.atlassian.renderer.v2.components.HtmlEscaper;
import com.atlassian.webresource.api.assembler.PageBuilderService;

public class MermaidMacro implements Macro {

	private static final String[] UNMODIFIED = { "->>", "<<-", "->", "<-", "<=", "=>" };
	private static final String[] MODIFIED = { "-##", "##-", "-#", "#-", "#=", "=#" };
	private PageBuilderService pageBuilderService;

	public MermaidMacro(PageBuilderService pageBuilderService) {
		this.pageBuilderService = pageBuilderService;
	}

	@Override
	public String execute(Map<String, String> parameters, String body, ConversionContext context)
			throws MacroExecutionException {
		pageBuilderService.assembler().resources().requireContext("mermaid-plugin");
		String modifiedBody = StringUtils.replaceEach(body, UNMODIFIED, MODIFIED);
		String escapedBody = HtmlEscaper.escapeAll(modifiedBody, false);
		String finalBody = StringUtils.replaceEach(escapedBody, MODIFIED, UNMODIFIED);
		StringBuilder sb = new StringBuilder();
		sb.append("<div class=\"mermaid\">");
		sb.append(finalBody);
		sb.append("</div>");
		return sb.toString();
	}

	@Override
	public BodyType getBodyType() {
		return BodyType.PLAIN_TEXT;
	}

	@Override
	public OutputType getOutputType() {
		return OutputType.BLOCK;
	}

}
