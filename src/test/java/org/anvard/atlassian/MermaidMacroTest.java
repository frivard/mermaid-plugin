package org.anvard.atlassian;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;

import com.atlassian.webresource.api.assembler.PageBuilderService;
import com.atlassian.webresource.api.assembler.RequiredResources;
import com.atlassian.webresource.api.assembler.WebResourceAssembler;

public class MermaidMacroTest {

	private MermaidMacro macro;
	
	@Before
	public void setUp() {
		RequiredResources resources = mock(RequiredResources.class);
		WebResourceAssembler assembler = mock(WebResourceAssembler.class);
		PageBuilderService pageBuilder = mock(PageBuilderService.class);
		when(assembler.resources()).thenReturn(resources);
		when(pageBuilder.assembler()).thenReturn(assembler);
		macro = new MermaidMacro(pageBuilder);
	}
	
	@Test
	public void xssPrevention() throws Exception {
		String bad = "<script>window.alert('abcde')</script>";
		String output = macro.execute(null, bad, null);
		assertEquals("XSS vulnerability", output.lastIndexOf("<script>"), -1);
		assertEquals("XSS vulnerability", output.lastIndexOf("</script>"), -1);
	}
}
